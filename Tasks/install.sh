#!/bin/bash

ConfDirectory=${XDG_CONFIG_HOME:-"$HOME/.config"}/tasks

# Check any arguments
case $1 in
	-help)
		echo "Run the script as is or use the -uninstall flag to remove the script"
		exit 1
		;;
	-uninstall)
		sudo rm /usr/bin/tasks && rm -r $ConfDirectory
		exit 1
		;;
esac

# Move file to /usr/bin and create conf directory and file
sudo cp ./tasks /usr/bin
sudo chmod +x /usr/bin/tasks
mkdir $ConfDirectory
touch $ConfDirectory/config

notify-send "Task" "Install successful. Add tasks to $ConfDirectory/config"
